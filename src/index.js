import "./scss/index.scss";
import Glide from '@glidejs/glide';

window.addEventListener('load',()=>{
    new Glide('.glide',{
        type: 'slider',
        perView: 3,
        gap: 20,
        rewind: false,
        breakpoints: {
            1400: {
                perView: 2
            },
            1024: {
                perView: 1
            }
        }
    }).mount()
})
